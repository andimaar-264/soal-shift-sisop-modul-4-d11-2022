#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>

static const char *dirPath = "/home/wicaksono/Documents";
char animeku[10] = "Animeku_";

void encode1(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
        return;

    int strLength = strlen(str);
    for (int i = 0; i < strLength; i++){
        if (str[i] == '.')
            break;
        if (str[i] == '/')
            continue;
        if (str[i] >= 65 && str[i] <= 90)
            str[i] = 90 + 65 - str[i];
        if (str[i] >= 97 && str[i] <= 122)
            str[i] = 97 + (str[i] - 97 + 13) % 26;
    }
}

void decode1(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL)
        return;

    int strLength = strlen(str), s = 0;
    for (int i = strLength; i >= 0; i--){
        if (str[i] == '/')
            break;
        if (str[i] == '.'){
            strLength = i;
            break;
        }
    }
    for (int i = 0; i < strLength; i++){
        if (str[i] == '/'){
            s = i + 1;
            break;
        }
    }
    for (int i = s; i < strLength; i++){
        if (str[i] == '/')
            continue;
        if (str[i] >= 65 && str[i] <= 90)
            str[i] = 90 + 65 - str[i];
        if (str[i] >= 97 && str[i] <= 122)
            str[i] = 97 + (str[i] - 97 + 13) % 26;
    }
}

void writeLog(const char *old, char *new, int type){
    char path[100];
    strcpy(path, dirPath);
    strcat(path, "/Wibu.log");

    FILE *fp = fopen(path, "a");
    if (type == 0)
        fprintf(fp, "MKDIR\tterenkripsi\t%s\t->\t%s\n", old, new);
    else if (type == 1)
        fprintf(fp, "RENAME\tterenkripsi\t%s\t->\t%s\n", old, new);
    else if (type == 2)
        fprintf(fp, "RENAME\tterdecode\t%s\t->\t%s\n", old, new);
    fclose(fp);
}

static int xmp_getattr(const char *path, struct stat *stbuf){
    char *strCheck = strstr(path, animeku);

    if (strCheck != NULL)
        decode1(strCheck);

    int res;
    char filePath[1000];

    strcpy(filePath, dirPath);
    strcat(filePath, path);
    res = lstat(filePath, stbuf);

    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    char *strCheck = strstr(path, animeku);
    if (strCheck != NULL)
        decode1(strCheck);

    char filePath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        strcpy(filePath, path);
    }
    else {
        strcpy(filePath, dirPath);
        strcat(filePath, path);
    }

    int res = 0;
    DIR *dp;
    struct dirent *dir;
    (void)offset;
    (void)fi;

    dp = opendir(filePath);
    if (dp == NULL)
        return -errno;

    while ((dir = readdir(dp)) != NULL){ 
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;
        if (strCheck != NULL)
            encode1(dir->d_name);

        res = (filler(buf, dir->d_name, &st, 0));
        if (res != 0)
            break;
    }
    closedir(dp);
    return 0;
}

static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char *strCheck = strstr(path, animeku);
    if (strCheck != NULL)
        decode1(strCheck);

    char filePath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        strcpy(filePath, path);
    }
    else {
        strcpy(filePath, dirPath);
        strcat(filePath, path);
    }

    int res = 0;
    int fd = 0;
    (void)fi;

    fd = open(filePath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static int xmp_mkDir(const char *path, mode_t mode){
    char filePath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        strcpy(filePath, path);
    }
    else {
        strcpy(filePath, dirPath);
        strcat(filePath, path);
    }

    char *folderPath = strstr(path, animeku);

    if (folderPath != NULL)
        writeLog(filePath, filePath, 0);

    int res = mkdir(filePath, mode);

    if (res == -1)
        return -errno;
    return 0;
}

static int xmp_unlink(const char *path){
    char *strCheck = strstr(path, animeku);
    if (strCheck != NULL)
        decode1(strCheck);

    char filePath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        strcpy(filePath, path);
    }
    else {
        strcpy(filePath, dirPath);
        strcat(filePath, path);
    }

    int res = unlink(filePath);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_rmDir(const char *path){
    char *strCheck = strstr(path, animeku);

    if (strCheck != NULL)
        decode1(strCheck);

    char filePath[1000];

    strcpy(filePath, dirPath);
    strcat(filePath, path);

    int res;
    res = rmdir(filePath);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_rename(const char *sPath, const char *dPath){
    char filePath[1000], fileDest[1000];

    strcpy(filePath, dirPath);
    strcat(filePath, sPath);
    strcpy(fileDest, dirPath);
    strcat(fileDest, dPath);

    if (strstr(fileDest, animeku) != NULL)
        writeLog(filePath, fileDest, 1);
    else if (strstr(filePath, animeku) != NULL)
        writeLog(filePath, fileDest, 2);

    int res;
    res = rename(filePath, fileDest);
    if (res == -1)
        return -errno;

    return 0;
}

static int xmp_open(const char *path, struct fuse_file_info *fi){
    char *strCheck = strstr(path, animeku);
    if (strCheck != NULL)
        decode1(strCheck);

    char filePath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        strcpy(filePath, path);
    }
    else {
        strcpy(filePath, dirPath);
        strcat(filePath, path);
    }
    
    int res;
    res = open(filePath, fi->flags);
    if (res == -1)
        return -errno;
    close(res);
    return 0;
}

static int xmp_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi){
    char *strCheck = strstr(path, animeku);
    if (strCheck != NULL)
        decode1(strCheck);

    char filePath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        strcpy(filePath, path);
    }
    else {
        strcpy(filePath, dirPath);
        strcat(filePath, path);
    }

    int res;
    int fd;
    (void)fi;

    fd = open(filePath, O_WRONLY);
    if (fd == -1)
        return -errno;

    res = pwrite(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static int xmp_fsync(const char *path, int isdatasync, struct fuse_file_info *fi){
    (void)path;
    (void)isdatasync;
    (void)fi;
    return 0;
}

static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkDir,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmDir,
    .rename = xmp_rename,
    .open = xmp_open,
    .write = xmp_write,
    .fsync = xmp_fsync,
};

int main(int argc, char *argv[]){
    umask(0);
    return fuse_main(argc, argv, &xmp_oper, NULL);
}
