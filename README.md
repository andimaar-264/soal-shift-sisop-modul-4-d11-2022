# soal shift sisop modul 4 d11 2022

## Members
1. Benedictus Bimo C W - 5025201097
2. Hasna Lathifah Purbaningdyah - 5025201108
3. Muhammad Andi Akbar Ramadhan - 5025201264

## Penyelesaian

Untuk `fuse_operations` `xmp_oper` sendiri adalah sebagai berikut,

```
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkDir,
    .unlink = xmp_unlink,
    .rmdir = xmp_rmDir,
    .rename = xmp_rename,
    .open = xmp_open,
    .write = xmp_write,
    .fsync = xmp_fsync,
};

```

Berikut merupakan fungsi untuk encode dan decode. Untuk setiap file didalam folder Documents, akan dilakukan encode/decode sesuai dengan awalan nama folder. Jika awalan nama folder adalah "Animeku_" maka semua file dan folder serta file didalam folder tersebut akan diencode. Sedangkan jika nama folder tidak berawalan "Animeku_", maka semua file dan folder serta file didalam folder tersebut akan didecode.
```
void encode1(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0)
        return;

    int strLength = strlen(str);
    for (int i = 0; i < strLength; i++){
        if (str[i] == '.')
            break;
        if (str[i] == '/')
            continue;
        if (str[i] >= 65 && str[i] <= 90)
            str[i] = 90 + 65 - str[i];
        if (str[i] >= 97 && str[i] <= 122)
            str[i] = 97 + (str[i] - 97 + 13) % 26;
    }
}

void decode1(char *str){
    if (strcmp(str, ".") == 0 || strcmp(str, "..") == 0 || strstr(str, "/") == NULL)
        return;

    int strLength = strlen(str), s = 0;
    for (int i = strLength; i >= 0; i--){
        if (str[i] == '/')
            break;
        if (str[i] == '.'){
            strLength = i;
            break;
        }
    }
    for (int i = 0; i < strLength; i++){
        if (str[i] == '/'){
            s = i + 1;
            break;
        }
    }
    for (int i = s; i < strLength; i++){
        if (str[i] == '/')
            continue;
        if (str[i] >= 65 && str[i] <= 90)
            str[i] = 90 + 65 - str[i];
        if (str[i] >= 97 && str[i] <= 122)
            str[i] = 97 + (str[i] - 97 + 13) % 26;
    }
}
```

Untuk setiap pemeriksaan nama foldernya, akan dilakukan perulangan while sehingga file didalam folder akan diiterasi 1 per 1 hingga semuanya selesai diiterasi. Untuk setiap iterasinya juga akan dilakukan pemeriksaan apakah folder tersebut mengandung awalan "Animeku_" atau tidak. Jika iya, maka akan dilakukan pemanggilan fungsi encode. Selanjutnya juga berlaku hal yang kurang lebih sama untuk `xmp_read`, `xmp_mkDir`, `xmp_unlink`, `xmp_write`, `xmp_open`, dan `xmp_rmDir`, dimana untuk fungsi-fungsi tersebut kurang lebih akan dilakukan pemanggilan fungsi `writeLog` untuk menulis log setiap terjadi perubahan baik encode/decode dan melakukan decode untuk folder yang tidak berawalan `Animeku_`.
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi){
    char *strCheck = strstr(path, animeku);
    if (strCheck != NULL)
        decode1(strCheck);

    char filePath[1000];
    if (strcmp(path, "/") == 0){
        path = dirPath;
        strcpy(filePath, path);
    }
    else {
        strcpy(filePath, dirPath);
        strcat(filePath, path);
    }

    int res = 0;
    DIR *dp;
    struct dirent *dir;
    (void)offset;
    (void)fi;

    dp = opendir(filePath);
    if (dp == NULL)
        return -errno;

    while ((dir = readdir(dp)) != NULL){ 
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = dir->d_ino;
        st.st_mode = dir->d_type << 12;
        if (strCheck != NULL)
            encode1(dir->d_name);

        res = (filler(buf, dir->d_name, &st, 0));
        if (res != 0)
            break;
    }
    closedir(dp);
    return 0;
}

```

Kemudian terdapat juga `xmp_rename` untuk melakukan pemeriksaan pada folder yang di rename pada Documents. Untuk setiap folder yang direname, akan dilakukan pemeriksaan apakah folder tersebut mengandung awalan `Animeku_` atau tidak. Kemudian program akan menentukan apakah folder tersebut perlu diencode/didecode

```
static int xmp_rename(const char *sPath, const char *dPath){
    char filePath[1000], fileDest[1000];

    strcpy(filePath, dirPath);
    strcat(filePath, sPath);
    strcpy(fileDest, dirPath);
    strcat(fileDest, dPath);

    if (strstr(fileDest, animeku) != NULL)
        writeLog(filePath, fileDest, 1);
    else if (strstr(filePath, animeku) != NULL)
        writeLog(filePath, fileDest, 2);

    int res;
    res = rename(filePath, fileDest);
    if (res == -1)
        return -errno;

    return 0;
}
```


**Demo Program**

![Window kiri merupakan Documents dan Window kanan merupakan folder fuse Dir](images/1.png)<br>
*Window kiri merupakan Documents dan Window kanan merupakan folder fuse Dir*

![Hasil encode file](images/2.png)<br>
*Hasil encode file*

![File yang tidak diencode karena tidak berawalan Animeku_](images/3.png)<br>
*File yang tidak diencode karena tidak berawalan Animeku_*

![File yang tidak diencode karena tidak berawalan Animeku_ tadi direname berawalan Animeku_ dan terencode](images/4.png)<br>
*File yang tidak diencode karena tidak berawalan Animeku_ tadi direname berawalan Animeku_ dan terencode*

**Kendala Pengerjaan**

Kendala pada pengerjaan ini adalah kurangnya pengalaman dan ilmu saya sehingga saya sempat mengalami beberapa tantangan, salah satunya adalah penulisan `Wibu.log`.